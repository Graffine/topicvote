import os

from flask import render_template
from topicvote import init_app
from topicvote.model.topic import inMemoryTopic
from topicvote.lib.sort import sort_topics

app_template = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'topicvote', 'templates')
app = init_app(app_template)

@app.route('/')
def home():
    sorted_topics = sort_topics(inMemoryTopic.values(), True)[:20]
    return render_template("index.html", topics=sorted_topics)

if __name__ == '__main__':
    app.run(port=3310)
