
inMemoryTopic = {}

class Topic(object):
    def __init__(self, title, vote):
        self.title = title
        self.vote = vote

    def updateVotes(self, count):
        self.vote = self.vote + count
        if self.vote < 0:
            self.vote = 0

