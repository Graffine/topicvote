# decorator.py

import traceback

from flask import abort
from functools import wraps
from topicvote.lib.exception import BadRequest, NotFound, InternalServerError

def exception():
    def decorator(func):
        @wraps(func)
        def wrapper(*arg, **kwarg):
            try:
                result = func(*arg, **kwarg)
                return result
            except NotFound, e:
                tb = traceback.format_exc()
                print('%s\n%s' % (e, tb))
                abort(404, 'Cannot found resource you requested')
            except BadRequest, e:
                tb = traceback.format_exc()
                print('%s\n%s' % (e.description, tb))
                abort(400, e.description)
            except InternalServerError, e:
                tb = traceback.format_exc()
                print('%s\n%s' % (e.description, tb))
                abort(500)
        return wrapper
    return decorator

