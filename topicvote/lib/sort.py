def sort_topics(topics, reverse=False):
    if type(topics) is list:
        return sorted(topics, key=lambda topic: topic.vote, reverse=reverse)
    else:
        raise Exception("Input topic type is not list but {}".format(type(topics)))
