from flask import Blueprint
from flask import abort, jsonify, request
from topicvote.lib.decorator import exception
from topicvote.lib.exception import BadRequest, NotFound
from topicvote.model.topic import inMemoryTopic, Topic

__all__ = ['TopicController']

TopicController = Blueprint('topic', __name__)

@TopicController.route('/topic', methods=['POST'])
@exception()
def create_topic():
    jsonData = request.json
    if 'title' not in jsonData or not jsonData['title']:
        raise BadRequest("Topic has no title")

    title = jsonData['title'].strip()
    if title in inMemoryTopic:
        raise BadRequest("Topic already existed")
    if not title or len(title) > 255:
        raise BadRequest('Topic title is empty or length of topic title exceeds 255 characters')

    inMemoryTopic[title] = Topic(title, 0)
    return ''

@TopicController.route('/topic/count', methods=['PUT'])
@exception()
def update_topic():
    jsonData = request.json
    if 'title' not in jsonData or 'vote_type' not in jsonData:
        raise BadRequest("Topic has no title")

    title = jsonData['title'].strip()
    vote_type = jsonData['vote_type']
    if title not in inMemoryTopic:
       raise NotFound
    if not title or len(title) > 255:
        raise BadRequest('Topic title is empty or length of topic title exceeds 255 characters')
    if vote_type not in ["upvote", "downvote"]:
        raise BadRequest('Illegal operation')

    topic = inMemoryTopic[title]
    count = {
        "upvote": 1,
        "downvote": -1
    }[vote_type]

    topic.updateVotes(count)

    return jsonify({'title': topic.title, 'vote': topic.vote})
