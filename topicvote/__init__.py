# __init__.py

from flask import Flask
from topicvote.controller import *

def init_app(template_folder):
    app = Flask(__name__, template_folder=template_folder)
    app.register_blueprint(TopicController)

    return app
