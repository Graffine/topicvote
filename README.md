# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Summary ##

* Environment build-up
    * Python runtime
    * Virtualenv
* How to run

## Environment build-up ##
### Python runtime ###
This project should run on python 2.7.13.

You could find it from here: [https://www.python.org/downloads/](https://www.python.org/downloads/)

### Virtualenv ###
You could use virtualenv to create your environment for testing purpose.

    sudo pip install virtualenv
    cd /project/root/path
    virtualenv [-p your python runtime] venv
    source venv/bin/activate

Now you are in virtual envionement. You could install project dependency like following

    pip install -r requirement.txt

## How to run ##
You could run it by python cli or gunicorn

    cd /project/root/path
    python run.py

or

    cd /project/root/path
    gunicorn run:app